use anyhow::Result;
use qdrant_client::prelude::*;
use qdrant_client::qdrant::{
    Condition, CreateCollection, Filter, SearchPoints, VectorParams, VectorsConfig,
};
use serde_json::json;

#[tokio::main]
async fn main() -> Result<()> {
    // Example of top level client
    // You may also use tonic-generated client from `src/qdrant.rs`
    let client = QdrantClient::from_url("http://localhost:6334").build()?;

    // Define the name of the collection
    let collection_name = "people";

    // Delete the collection if it already exists
    if client.collection_info(collection_name).await.is_ok() {
        client.delete_collection(collection_name).await?;
    }

    // Create the collection
    client
        .create_collection(&CreateCollection {
            collection_name: collection_name.into(),
            vectors_config: Some(VectorsConfig {
                config: Some(qdrant_client::qdrant::vectors_config::Config::Params(
                    VectorParams {
                        size: 10,
                        distance: Distance::Cosine.into(),
                        ..Default::default()
                    },
                )),
            }),
            ..Default::default()
        })
        .await?;

    // Ingest hardcoded entries of people into the Qdrant collection
    let hardcoded_entries = vec![
        (1, "Ayush", 25),
        (2, "Alice", 26),
        (3, "Bob", 22),
        (4, "Emily", 28),
        (5, "Michael", 35),
        (6, "Ajay", 25),
    ];

    for (id, name, age) in hardcoded_entries {
        let mut vector = vec![0.0; 10];
        vector[0] = age as f32;
        let payload: Payload = json!({
            "id": id,
            "name": name,
            "age": age,
        })
        .try_into()
        .unwrap();
        let point = PointStruct::new(id, vector, payload);
        client
            .upsert_points_blocking(collection_name, None, vec![point], None)
            .await?;
    }

    
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![0.0; 10], // Vector of size 10 filled with zeros
            filter: Some(Filter::all(vec![Condition::matches("age", 25)])),
            limit: 2,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await?;
    println!("The people with age 25 are:");
    
    for result in &search_result.result {
        println!("Name: {}", result.payload.get("name").unwrap().as_str().unwrap());
    }

    

    Ok(())
}


