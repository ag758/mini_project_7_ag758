use reqwest::Client;
use rand::{Rng, thread_rng};

pub async fn ingest_random_data(client: &Client, count: usize) -> Result<(), reqwest::Error> {
    for _ in 0..count {
        let id = thread_rng().gen::<u32>();
        let name = format!("Person {}", id);
        let age = thread_rng().gen_range(18..=80);
        let university = if age < 25 {
            "University A"
        } else {
            "University B"
        };
        
        let data = format!(r#"{{"id": {}, "attributes": {{"name": "{}", "age": {}, "university": "{}"}}}}"#, id, name, age, university);
        
        client.post("http://localhost:6333/v1/collection/items")
            .body(data)
            .send()
            .await?;
    }
    
    Ok(())
}

