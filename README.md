# Qdrant Search (Week 7)

The Qdrant People Search project in Rust illustrates the utilization of Qdrant, an approximate nearest neighbor search engine, for searching individuals based on their age. This Rust project exemplifies the integration of Qdrant within an application, emphasizing the creation of a collection of people and the querying of individuals based on their age.

## Introduction

In this project, Qdrant serves as a robust solution for conducting similarity searches on extensive datasets. Our focus lies in leveraging Qdrant to establish a comprehensive collection of individuals, each characterized by a vector encapsulating their age. Through the ingestion of this dataset into Qdrant, we unlock the capability to conduct swift and efficient queries within the collection, enabling us to pinpoint individuals who align with predefined criteria, such as age.

## Features

- **Creating a Collection**: Within the project, a collection titled "people" is established in Qdrant. This collection is meticulously configured to house vectors representing individuals, each vector encapsulating their age.
  
- **Ingesting Data**: The project seamlessly integrates hardcoded entries of individuals into the Qdrant collection. These entries include essential details such as ID, name, and age, with the age of each individual forming a crucial component of their corresponding vector.
  
- **Searching by Age**: Leveraging Qdrant's capabilities, the project executes a targeted search within the collection to identify individuals of a specific age. In this instance, the search is directed towards individuals aged 25, showcasing the precision and efficiency of Qdrant's search functionality.

## Installation

1. Clone the repository:

   ```
   git clone https://gitlab.com/ag758/mini_project_7_ag758.git
   ```


2. Build and run the project using Cargo:

   ```
   cargo run
   ```

   Ensure that you have a Qdrant server running locally at `http://localhost:6334` or update the URL accordingly in the code.

## Usage

1. Modify the `main.rs` file to customize the hardcoded entries of people, including their ID, name, and age.
  
2. Adjust the search parameters in the `main.rs` file to perform searches based on different age criteria or other attributes.

3. Run the project using Cargo:

   ```
   cargo run
   ```

4. View the output to see the names of people who match the specified age criteria.

## Dependencies

This project relies on the following dependencies:

- [qdrant-client](https://crates.io/crates/qdrant-client): A Rust client library for Qdrant.
- [serde_json](https://crates.io/crates/serde_json): A JSON serialization library for Rust.

These dependencies are managed via Cargo and will be installed automatically when building the project.

## Screenshots

![Query](screenshots/query_results.png)